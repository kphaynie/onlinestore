﻿using Microsoft.AspNetCore.Mvc;
using OnlineStore.Models;
using System.Collections.Generic;
using System.Linq;

namespace OnlineStore.Controllers
{
    public class ProductsController : Controller
    {
        private readonly OnlineStoreContext _context;

        public ProductsController(OnlineStoreContext context)
        {
            _context = context;
        }

        public ViewResult List()
        {
            return View(new ProductsListViewModel
            {
                Products = _context.Products.OrderBy(p => p.Id)
            }); ;
        }

        public ViewResult Details(int productId)
        {
            var product = _context.Products.FirstOrDefault(p => p.Id == productId);

            if ( product == null )
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            return View(product);
        }
    }
}
