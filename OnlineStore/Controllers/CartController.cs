﻿using Microsoft.AspNetCore.Mvc;
using OnlineStore.Models;
using System.Linq;

namespace OnlineStore.Controllers
{
    public class CartController : Controller
    {
        private readonly OnlineStoreContext _context;
        private readonly Cart _shoppingCart;

        public CartController(OnlineStoreContext context, Cart shoppingCart)
        {
            _context = context;
            _shoppingCart = shoppingCart;
        }

        public ViewResult Index()
        {
            var items = _shoppingCart.GetCartContents();
            _shoppingCart.CartItems = items;

            var cartViewModel = new CartViewModel
            {
                ShoppingCart = _shoppingCart,
                TotalPrice = _shoppingCart.GetTotalCost(),
                TotalTaxes = _shoppingCart.GetTotalTaxes()
            };

            return View(cartViewModel);
        }

        public ViewResult Checkout()
        {
            var items = _shoppingCart.GetCartContents();
            _shoppingCart.CartItems = items;

            var cartViewModel = new CartViewModel
            {
                ShoppingCart = _shoppingCart,
                TotalPrice = _shoppingCart.GetTotalCost(),
                TotalTaxes = _shoppingCart.GetTotalTaxes()
            };

            return View(cartViewModel);
        }

        public RedirectToActionResult AddToCart(int productId)
        {
            var selectedProduct = _context.Products.FirstOrDefault(p => p.Id == productId);
            if (selectedProduct != null)
            {
                _shoppingCart.AddToCart(selectedProduct, 1);
            }

            return RedirectToAction("List","Products");
        }

        public RedirectToActionResult EmptyCart()
        {
            _shoppingCart.EmptyCart();

            return RedirectToAction("List", "Products");
        }

        public ViewResult ThankYou()
        {
            _shoppingCart.EmptyCart();

            return View();
        }

        public RedirectToActionResult RemoveFromCart(int productId)
        {
            var selectedProduct = _context.Products.FirstOrDefault(p => p.Id == productId);
            if (selectedProduct != null)
            {
                _shoppingCart.RemoveFromCart(selectedProduct);
            }

            return RedirectToAction("Index");
        }
    }
}