﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace OnlineStore.Models
{
    public class Catalog
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new OnlineStoreContext(serviceProvider.GetRequiredService<DbContextOptions<OnlineStoreContext>>()))
            {
                if (context.Products.Any())
                {
                    return;
                }

                context.Products.AddRange(
                    new Product
                    {
                        Name = "16lb bag of Skittles",
                        TaxExempt = true,
                        IsImport = false,
                        Price = 16.00m,
                        Category = "Grocery",
                        SKU = 11111111,
                        Description = "A comically large bag of Skittles. Guaranteed 75% green."
                    },
                    new Product
                    {
                        Name = "Walkman",
                        TaxExempt = false,
                        IsImport = false,
                        Price = 99.99m,
                        Category = "Electronics",
                        SKU = 22222222,
                        Description = "Perfect for those stuck in the 1980s. Cassettes sold separately."
                    },
                    new Product
                    {
                        Name = "bag of microwave Popcorn",
                        TaxExempt = true,
                        IsImport = false,
                        Price = 0.99m,
                        Category = "Grocery",
                        SKU = 33333333,
                        Description = "One. Single. Bag. Unpopped, unbuttered."
                    },
                    new Product
                    {
                        Name = "bag of Vanilla-Hazelnut Coffee",
                        TaxExempt = true,
                        IsImport = true,
                        Price = 11.00m,
                        Category = "Grocery",
                        SKU = 44444444,
                        Description = "Better than a scented candle."
                    },
                    new Product
                    {
                        Name = "Vespa",
                        TaxExempt = false,
                        IsImport = true,
                        Price = 15001.25m,
                        Category = "Vehicle",
                        SKU = 55555555,
                        Description = "Perfect companion to the Walkman to complete your fresh-out-of-the-80's look."
                    },
                    new Product
                    {
                        Name = "crate of Almond Snickers",
                        TaxExempt = true,
                        IsImport = true,
                        Price = 75.99m,
                        Category = "Grocery",
                        SKU = 66666666,
                        Description = "You're not you when you're hungry. You're also not you when you buy an entire crate of Snickers."
                    },
                    new Product
                    {
                        Name = "Discman",
                        TaxExempt = false,
                        IsImport = false,
                        Price = 55.00m,
                        Category = "Electronics",
                        SKU = 77777777,
                        Description = "A decade ahead the Walkman, but still a decade or two behind."
                    },
                    new Product
                    {
                        Name = "Bottle of Wine",
                        TaxExempt = false,
                        IsImport = true,
                        Price = 10.00m,
                        Category = "Alcohol",
                        SKU = 88888888,
                        Description = "Generic wine. At least it's not Franzia."
                    },
                    new Product
                    {
                        Name = "300# bag of Fair-Trade Coffee",
                        TaxExempt = true,
                        IsImport = false,
                        Price = 997.99m,
                        Category = "Grocery",
                        SKU = 99999999,
                        Description = "300 pounds of economically positive coffee."
                    }
                ); ;
                context.SaveChanges();
            }
        }
    }
}
