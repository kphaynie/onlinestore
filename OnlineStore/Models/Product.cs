﻿using System.ComponentModel.DataAnnotations;

namespace OnlineStore.Models
{
    public class Product
    {
        private const decimal SALES_TAX_RATE = 0.10m;
        private const decimal IMPORT_DUTY_RATE = 0.05m;

        public int Id { get; set; }
        [Display(Name = "Product Name")]
        public string Name { get; set; }
        public string Category { get; set; }
        [Required]
        public int SKU { get; set; }
        [Display(Name = "Imported?")]
        public bool IsImport { get; set; }
        [Display(Name = "Tax Exempt?")]
        [Required]
        public bool TaxExempt { get; set; }
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            var localString = "";

            if( IsImport )
            {
                localString = "Imported ";
            }

            localString += Name;

            return localString;
        }

        public decimal SalesTaxPerQty()
        {
            if (TaxExempt) return 0.00m;

            return Price * SALES_TAX_RATE;
        }

        public decimal ImportDutyPerQty()
        {
            if (!IsImport) return 0.00m;

            return Price * IMPORT_DUTY_RATE;
        }
    }
}
