﻿using System;

namespace OnlineStore.Models
{
    public class CartItem
    {
        public int CartItemId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public string CartId { get; set; }

        public decimal SalesTax()
        {
            return Math.Ceiling(Product.SalesTaxPerQty() * Quantity * 20) / 20;
        }

        public decimal ImportDuty()
        {
            return Math.Ceiling(Product.ImportDutyPerQty() * Quantity * 20) / 20;
        }

        public decimal TotalTaxes()
        {
            return SalesTax() + ImportDuty();
        }

        public decimal TotalPrice()
        {
            return Product.Price * Quantity;
        }

        public decimal TotalPriceWithTaxes()
        {
            return TotalPrice() + TotalTaxes();
        }

        public override string ToString()
        {
            return string.Format("{0} {1}: {2:C2}", Quantity, Product.ToString(), TotalPriceWithTaxes());
        }
    }
}
