﻿namespace OnlineStore.Models
{
    public class CartViewModel
    {
        public Cart ShoppingCart { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalTaxes { get; set; }
    }
}
