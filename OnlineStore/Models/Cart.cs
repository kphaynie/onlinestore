﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OnlineStore.Models
{
    public class Cart
    {
        private OnlineStoreContext _context;
        private Cart(OnlineStoreContext context)
        {
            _context = context;
        }

        public string CartId { get; set; }
        public List<CartItem> CartItems { get; set; }

        public static Cart GetCart(IServiceProvider serviceProvider)
        {
            ISession session = serviceProvider.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;

            var context = new OnlineStoreContext(serviceProvider.GetRequiredService<DbContextOptions<OnlineStoreContext>>());
            string cartId = session.GetString("CartId") ?? Guid.NewGuid().ToString();

            session.SetString("CartId", cartId);

            return new Cart(context) { CartId = cartId };
        }

        public void AddToCart(Product product, int quantity)
        {
            var cartItem = _context.CartItems.SingleOrDefault(ci => ci.Product.Id == product.Id && ci.CartId == CartId);

            if (cartItem == null)
            {
                cartItem = new CartItem
                {
                    CartId = CartId,
                    Product = product,
                    Quantity = quantity
                };

                _context.CartItems.Add(cartItem);
            } else
            {
                cartItem.Quantity++;
            }

            /** ??? For some reason, it's tracking a local change to a product and throwing a DB error for
             * trying to make an update to a DB with ID INSERT disabled. Clearing the local cache as a hacky
             * solution 'til I have time to look into the real reason it is happening. **/
            _context.Products.Local.Clear();
            _context.SaveChanges();
        }

        public int RemoveFromCart(Product product)
        {
            var cartItem = _context.CartItems.SingleOrDefault(ci => ci.Product.Id == product.Id && ci.CartId == CartId);

            var localQuantity = 0;

            if (cartItem != null)
            {
                if (cartItem.Quantity > 1 )
                {
                    cartItem.Quantity--;
                    localQuantity = cartItem.Quantity;
                } else
                {
                    _context.CartItems.Remove(cartItem);
                }
            }

            /** ??? For some reason, it's tracking a local change to a product and throwing a DB error for
             * trying to make an update to a DB with ID INSERT disabled. Clearing the local cache as a hacky
             * solution 'til I have time to look into the real reason it is happening. **/
            _context.Products.Local.Clear(); 
            _context.SaveChanges();

            return localQuantity;
        }

        public void EmptyCart()
        {
            var cartItems = _context
                .CartItems
                .Where(cart => cart.CartId == CartId);

            _context.CartItems.RemoveRange(cartItems);

            _context.SaveChanges();
        }

        public decimal GetTotalCost()
        {
            var total = _context.CartItems.Where(ci => ci.CartId == CartId)
                .Select(ci => ci.TotalPriceWithTaxes()).Sum();
            return total;
        }

        public decimal GetTotalTaxes()
        {
            var total = _context.CartItems.Where(ci => ci.CartId == CartId)
                .Select(ci => ci.TotalTaxes()).Sum();
            return total;
        }

        public List<CartItem> GetCartContents()
        {
            return CartItems ??
                   (CartItems = _context.CartItems.Where(ci => ci.CartId == CartId)
                           .Include(ci => ci.Product)
                           .ToList());
        }
    }
}
