﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace OnlineStore.Models
{
    public class OnlineStoreContext : DbContext
    {
        public OnlineStoreContext (DbContextOptions<OnlineStoreContext> options)
            : base(options)
        {
        }

        public DbSet<OnlineStore.Models.Product> Products { get; set; }
        public DbSet<OnlineStore.Models.CartItem> CartItems { get; set; }
    }
}
