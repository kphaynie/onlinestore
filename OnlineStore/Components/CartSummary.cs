﻿using Microsoft.AspNetCore.Mvc;
using OnlineStore.Models;

namespace OnlineStore.Components
{
    public class CartSummary:ViewComponent
    {
        private readonly Cart _shoppingCart;

        public CartSummary (Cart shoppingCart)
        {
            _shoppingCart = shoppingCart;
        }

        public IViewComponentResult Invoke()
        {
            var cartItems = _shoppingCart.GetCartContents();
            _shoppingCart.CartItems = cartItems;

            var cartViewModel = new CartViewModel
            {
                ShoppingCart = _shoppingCart,
                TotalPrice = _shoppingCart.GetTotalCost(),
                TotalTaxes = _shoppingCart.GetTotalTaxes()
            };

            return View(cartViewModel);
        }
    }
}
