using OnlineStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTestOnlineStore
{
    public class TestOnlineStore
    {
        /* These tests don't sufficiently test some of the core features of Carts, such as Adding and Removing items and demonstrating
         * calculation of totals of taxes and import duties. Ideally, we would create a mock OnlineStoreContext context object that could be used for testing a wide
         * array of different components of this application, but to do so right now would be highly time consuming. 
         * Maybe something along these lines? https://stackoverflow.com/questions/25960192/mocking-ef-dbcontext-with-moq or
         * https://blog.learningtree.com/mock-entity-frameworks-dbcontext-unit-testing/ */

        [Fact]
        public void TestProductTaxExempt()
        {
            //Arrange
            Product productOne = Products.SingleOrDefault(p => p.Id == 1);
            Product productTwo = Products.SingleOrDefault(p => p.Id == 2);

            //Act
            decimal taxesOne = productOne.SalesTaxPerQty();
            decimal taxesTwo = productTwo.SalesTaxPerQty();

            //Assert
            Assert.Equal(0.00m, taxesOne);
            Assert.Equal(0.00m, taxesTwo);
        }

        [Fact]
        public void TestProductNonImport()
        {
            //Arrange
            Product productOne = Products.SingleOrDefault(p => p.Id == 1);
            Product productTwo = Products.SingleOrDefault(p => p.Id == 3);

            //Act
            decimal taxesOne = productOne.ImportDutyPerQty();
            decimal taxesTwo = productTwo.ImportDutyPerQty();

            //Assert
            Assert.Equal(0.00m, taxesOne);
            Assert.Equal(0.00m, taxesTwo);
        }

        [Fact]
        public void TestProductNonTaxExempt()
        {
            //Arrange
            Product productOne = Products.SingleOrDefault(p => p.Id == 3);
            Product productTwo = Products.SingleOrDefault(p => p.Id == 4);

            //Act
            decimal taxesOne = productOne.SalesTaxPerQty();
            decimal taxesTwo = productTwo.SalesTaxPerQty();

            //Assert
            Assert.Equal(3.3330m, taxesOne);
            Assert.Equal(44.7660m, taxesTwo);
        }

        [Fact]
        public void TestProductImport()
        {
            //Arrange
            Product productOne = Products.SingleOrDefault(p => p.Id == 2);
            Product productTwo = Products.SingleOrDefault(p => p.Id == 4);

            //Act
            decimal taxesOne = productOne.ImportDutyPerQty();
            decimal taxesTwo = productTwo.ImportDutyPerQty();

            //Assert
            Assert.Equal(5.8885m, taxesOne);
            Assert.Equal(22.3830m, taxesTwo);
        }

        [Fact]
        public void TestCartItemTaxesAndPrice()
        {
            //Arrange
            CartItem cartItemOne = CartItems.SingleOrDefault(ci => ci.CartItemId == 1);
            CartItem cartItemTwo = CartItems.SingleOrDefault(ci => ci.CartItemId == 2);
            CartItem cartItemThree = CartItems.SingleOrDefault(ci => ci.CartItemId == 3);
            CartItem cartItemFour = CartItems.SingleOrDefault(ci => ci.CartItemId == 4);

            //Act
            decimal taxesOne = cartItemOne.TotalTaxes();
            decimal priceOne = cartItemOne.TotalPriceWithTaxes();

            decimal taxesTwo = cartItemTwo.TotalTaxes();
            decimal priceTwo = cartItemTwo.TotalPriceWithTaxes();

            decimal taxesThree = cartItemThree.TotalTaxes();
            decimal priceThree = cartItemThree.TotalPriceWithTaxes();

            decimal taxesFour = cartItemFour.TotalTaxes();
            decimal priceFour = cartItemFour.TotalPriceWithTaxes();

            //Assert
            Assert.Equal(0.00m, taxesOne);
            Assert.Equal(555.00m, priceOne);

            Assert.Equal(17.70m, taxesTwo);
            Assert.Equal(371.01m, priceTwo);

            Assert.Equal(23.35m, taxesThree);
            Assert.Equal(256.66m, priceThree);

            Assert.Equal(134.35m, taxesFour);
            Assert.Equal(1029.67m, priceFour);
        }

        #region Mocks
        private List<CartItem> CartItems
        {
            get
            {
                return new List<CartItem>
                {
                    new CartItem
                    {
                        CartItemId = 1,
                        Product = Products.FirstOrDefault(p => p.Id == 1),
                        Quantity = 5
                    },
                    new CartItem
                    {
                        CartItemId = 2,
                        Product = Products.FirstOrDefault(p => p.Id == 2),
                        Quantity = 3
                    },
                    new CartItem
                    {
                        CartItemId = 3,
                        Product = Products.FirstOrDefault(p => p.Id == 3),
                        Quantity = 7
                    },
                    new CartItem
                    {
                        CartItemId = 4,
                        Product = Products.FirstOrDefault(p => p.Id == 4),
                        Quantity = 2
                    }
                };
            }
        }

        private List<Product> Products
        {
            get
            {
                return new List<Product>
                {
                    new Product
                    {
                        Id = 1,
                        Name = "Test Product TaxExempt, NonImport",
                        TaxExempt = true,
                        IsImport = false,
                        Price = 111.00m,
                        Category = "Grocery",
                        SKU = 11111111,
                        Description = ""
                    },
                    new Product
                    {
                        Id = 2,
                        Name = "Test Product TaxExempt, Import",
                        TaxExempt = true,
                        IsImport = true,
                        Price = 117.77m,
                        Category = "Grocery",
                        SKU = 22222222,
                        Description = ""
                     },
                    new Product
                    {
                        Id = 3,
                        Name = "Test Product NonTaxExempt, NonImport",
                        TaxExempt = false,
                        IsImport = false,
                        Price = 33.33m,
                        Category = "Electronics",
                        SKU = 33333333,
                        Description = ""
                    },
                    new Product
                    {
                        Id = 4,
                        Name = "Test Product NonTaxExempt, Import",
                        TaxExempt = false,
                        IsImport = true,
                        Price = 447.66m,
                        Category = "Vehicle",
                        SKU = 44444444,
                        Description = ""
                    }
                };
            }
        }
        #endregion
    }
}
